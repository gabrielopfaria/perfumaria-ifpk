<?php

use App\Http\Controllers\HistoricoController;
use App\Http\Controllers\IngredienteController;
use App\Http\Controllers\PerfumeController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/','/login');

Route::middleware(['auth:sanctum', 'verified'])->group(function (){
    Route::get('/dashboard',[HistoricoController::class, 'index'])->name('dashboard');
    // Route::get('/dashboard', function () {
    //     return Inertia::render('Dashboard',['success'=>session('success')]);
    // })->name('dashboard');

    //INGREDIENTES
    Route::get('/ingredientes',[IngredienteController::class, 'index'])->name('ingrediente');
    Route::get('/ingredientes/novo',[IngredienteController::class, 'create'])->name('ingrediente.novo');
    Route::post('/ingredientes/novo',[IngredienteController::class, 'store']);
    Route::get('/ingredientes/{ingrediente}/adiciona',[IngredienteController::class, 'edit']);
    Route::post('/ingredientes/{ingrediente}/adiciona',[IngredienteController::class, 'update']);

    //PERFUMES
    Route::get('/perfumes',[PerfumeController::class,'index'])->name('perfumes');
    Route::get('/perfumes/novo',[PerfumeController::class, 'create'])->name('perfumes.novo');
    Route::post('/perfumes/novo',[PerfumeController::class, 'store']);
    Route::get('/perfumes/{perfume}/adiciona',[PerfumeController::class, 'edit']);
    Route::post('/perfumes/{perfume}/adiciona',[PerfumeController::class, 'update']);
});
