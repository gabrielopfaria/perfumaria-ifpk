<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perfume extends Model
{
    use HasFactory;
    protected $fillable = [
        'codigo',
        'nome'
    ];

    public function historicos () {
        return $this->hasMany(Historico::class,'perfume_id','id');
    }
}
