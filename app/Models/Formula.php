<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{
    use HasFactory;
    protected $fillable = [
        'perfume_id',
        'ingrediente_id',
        'porcentagem'
    ];

    public function ingredientes(){
        return $this->belongsTo(Ingrediente::class,'ingrediente_id','id');
    }
}
