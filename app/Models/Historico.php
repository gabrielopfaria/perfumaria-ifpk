<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    use HasFactory;
    protected $fillable = [
        'perfume_id',
        'ingrediente_id',
        'perfume_total',
        'fragancia_total'
    ];

}
