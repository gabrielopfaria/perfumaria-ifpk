<?php

namespace App\Http\Controllers;

use App\Models\Formula;
use App\Models\Historico;
use App\Models\Ingrediente;
use App\Models\Perfume;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PerfumeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perfumes = Perfume::with('historicos')->get();
        // dd($perfumes);
        return Inertia::render('Perfumes/Index', compact('perfumes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fragancias = Ingrediente::where('tipo', '1')->get();
        return Inertia::render('Perfumes/Create', compact('fragancias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $perfume = Perfume::create([
            'codigo' => $request->codigo,
            'nome' => $request->nome,
        ]);
        Formula::create([
            'perfume_id' => $perfume->id,
            'ingrediente_id' => 1,
            'porcentagem' => $request->agua
        ]);
        Formula::create([
            'perfume_id' => $perfume->id,
            'ingrediente_id' => 2,
            'porcentagem' => $request->alcool
        ]);
        Formula::create([
            'perfume_id' => $perfume->id,
            'ingrediente_id' => $request->fragancia['id'],
            'porcentagem' => $request->ingrediente
        ]);
        return redirect()->route('perfumes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Perfume  $perfume
     * @return \Illuminate\Http\Response
     */
    public function show(Perfume $perfume)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Perfume  $perfume
     * @return \Illuminate\Http\Response
     */
    public function edit(Perfume $perfume)
    {
        $estoque = Ingrediente::all();
        $formula = Formula::where('perfume_id',$perfume->id)->with('ingredientes')->get();
        return Inertia::render('Perfumes/Edit', compact('perfume','formula','estoque'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Perfume  $perfume
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Perfume $perfume)
    {
        $formula = Formula::where('perfume_id',$perfume->id)->get();
        $bd_agua = Ingrediente::where('id',1)->first();
        $bd_alcool = Ingrediente::where('id',2)->first();
        $bd_fragancia = Ingrediente::where('id',$formula[2]->ingrediente_id)->first();
        // dd($formula[2]->ingrediente_id);

        $agua_final = $request->ml*($formula[0]->porcentagem/100);
        $alcool_final = $request->ml*($formula[1]->porcentagem/100);
        $fragancia_final = $request->ml*($formula[2]->porcentagem/100);

        // dd($bd_fragancia->ml);

        $bd_agua->update([
            'ml' => $bd_agua->ml - $agua_final
        ]);
        $bd_alcool->update([
            'ml' => $bd_alcool->ml - $alcool_final
        ]);
        $bd_fragancia->update([
            'ml' => $bd_fragancia->ml - $fragancia_final
        ]);

        Historico::create([
            'perfume_id' => $perfume->id,
            'ingrediente_id' => $formula[2]->ingrediente_id,
            'perfume_total' => $request->ml,
            'fragancia_total' => $fragancia_final
        ]);

        return redirect()->route('perfumes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Perfume  $perfume
     * @return \Illuminate\Http\Response
     */
    public function destroy(Perfume $perfume)
    {
        //
    }
}
