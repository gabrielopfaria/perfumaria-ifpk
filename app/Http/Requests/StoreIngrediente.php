<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreIngrediente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo' => 'required | unique:ingredientes',
            'nome' => 'required',
            'ml' => 'required',
            'tipo' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'codigo.required' => 'O campo Código é de preenchimento obrigatório!',
            'codigo.unique' => 'Ingrediente já existe',
            'nome.required' => 'O campo Nome é de preenchimento obrigatório!',
            'ml.required' => 'O campo ml é de preenchimento obrigatório!',
            'tipo.required' => 'O campo tipo é de preenchimento obrigatório!',
        ];
    }
}
