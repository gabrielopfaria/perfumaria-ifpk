<?php

use App\Models\Ingrediente;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredientes', function (Blueprint $table) {
            $table->id();
            $table->integer('codigo')->unique();
            $table->string('nome');
            $table->decimal('ml');
            $table->boolean('tipo');
            $table->timestamps();
        });
        Ingrediente::create([
            'codigo' => 1,
            'nome' => 'Água',
            'ml' => 0,
            'tipo' => 0
        ]);
        Ingrediente::create([
            'codigo' => 2,
            'nome' => 'Álcool',
            'ml' => 0,
            'tipo' => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredientes');
    }
}
